# Toward a Research Goal: 
## Bayesian Model of Station-Based trends in CA Watersheds

In a paper that I am currently writing, I needed to do some Bayesian modeling of the statistics of precipitation in California watersheds.  My goal was to use a well-known and vetted station dataset: the [Global Historical Climatology Network Daily (GHCND)](https://www.ncdc.noaa.gov/data-access/land-based-station-data/land-based-datasets/global-historical-climatology-network-ghcn) dataset.  This therefore required being able to:

 1. find stations, within this global dataset, within CA watersheds;
 2. assign each station to the watershed that it is within; and
 3. carry out the statistical analysis on stations within each watershed.

One of the end results of this analysis is the following figure that I am using for the paper:

![Trends in mean precipitation in CA watersheds](https://bytebucket.org/taobrienlbl/maptimedavis/raw/master/fig_ca_huc6_mean_trends.png | width=400)
**Figure 1**: Trends in mean precipitation event magnitude (in % per century) within the 6-digit hydrologic units (HUC6 units) in California.

This notebook demonstrates how to approach steps (1) and (2) using a great tool for dealing with geographic data: `geopandas`.

# Prerequisites:

 1. A python distribution with the following libraries installed (see below for a simple suggestion on this):
  * [numpy](http://www.numpy.org/)
  * [pandas](https://pandas.pydata.org/)
  * [geopandas](http://geopandas.org/)
  * [shapely](https://pypi.python.org/pypi/Shapely)
  * [matplotlib](https://matplotlib.org/)
 2. The GHCND Station list file [`ghcnd-stations.txt`](https://bitbucket.org/taobrienlbl/maptimedavis/raw/master/ghcnd-stations.txt)
 3. The file containing the CA watershed boundaries: [`wbdhu8_a_ca.zip`](https://bitbucket.org/taobrienlbl/maptimedavis/raw/master/wbdhu8_a_ca.zip)
 
## Anaconda Python
The simplest way to get an appropriate Python distribution is to install the [Anaconda Python](https://www.anaconda.com/download/) distribution.  Once this has been downloaded and installed, install geopandas following [the directions on their website](http://geopandas.org/install.html).

## `ghcnd-stations.txt`: [http://bit.ly/2Dlp90z](http://bit.ly/2Dlp90z)

## `wbdhu8_a_ca.zip`: [http://bit.ly/2mUZS6B](http://bit.ly/2mUZS6B)

Once everything is installed properly, run the code below; it should run without issues.

```python
import pylab as PP
import matplotlib as mpl
import shapely.geometry 
import pandas as pd
import geopandas

# turn off numpy warnings
import warnings
warnings.filterwarnings('ignore')

# check that the required files are present
import os
assert os.path.exists("ghcnd-stations.txt"),"`ghcnd-stations.txt` not found.  Please download to the current directory from https://bitbucket.org/taobrienlbl/maptimedavis/raw/master/ghcnd-stations.txt" 
assert os.path.exists("wbdhu8_a_ca.zip"),"`ghcnd-stations.txt` not found.  Please download to the current directory from https://bitbucket.org/taobrienlbl/maptimedavis/raw/master/wbdhu8_a_ca.zip"

from numpy import *
```
